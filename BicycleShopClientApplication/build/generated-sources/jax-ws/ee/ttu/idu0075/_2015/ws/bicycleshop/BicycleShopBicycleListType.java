
package ee.ttu.idu0075._2015.ws.bicycleshop;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bicycleShopBicycleListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bicycleShopBicycleListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bicycleShopBicycle" type="{http://www.ttu.ee/idu0075/2015/ws/BicycleShop}bicycleShopBicycleType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bicycleShopBicycleListType", propOrder = {
    "bicycleShopBicycle"
})
public class BicycleShopBicycleListType {

    protected List<BicycleShopBicycleType> bicycleShopBicycle;

    /**
     * Gets the value of the bicycleShopBicycle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bicycleShopBicycle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBicycleShopBicycle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BicycleShopBicycleType }
     * 
     * 
     */
    public List<BicycleShopBicycleType> getBicycleShopBicycle() {
        if (bicycleShopBicycle == null) {
            bicycleShopBicycle = new ArrayList<BicycleShopBicycleType>();
        }
        return this.bicycleShopBicycle;
    }

}
