
package ee.ttu.idu0075._2015.ws.bicycleshop;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2015.ws.bicycleshop package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetBicycleShopResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "getBicycleShopResponse");
    private final static QName _AddBicycleShopBicycleResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "addBicycleShopBicycleResponse");
    private final static QName _AddBicycleResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "addBicycleResponse");
    private final static QName _GetBicycleShopBicycleListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "getBicycleShopBicycleListResponse");
    private final static QName _GetBicycleResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "getBicycleResponse");
    private final static QName _AddBicycleShopResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/BicycleShop", "addBicycleShopResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2015.ws.bicycleshop
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBicycleShopListRequest }
     * 
     */
    public GetBicycleShopListRequest createGetBicycleShopListRequest() {
        return new GetBicycleShopListRequest();
    }

    /**
     * Create an instance of {@link GetBicycleShopRequest }
     * 
     */
    public GetBicycleShopRequest createGetBicycleShopRequest() {
        return new GetBicycleShopRequest();
    }

    /**
     * Create an instance of {@link GetBicycleShopListResponse }
     * 
     */
    public GetBicycleShopListResponse createGetBicycleShopListResponse() {
        return new GetBicycleShopListResponse();
    }

    /**
     * Create an instance of {@link BicycleShopType }
     * 
     */
    public BicycleShopType createBicycleShopType() {
        return new BicycleShopType();
    }

    /**
     * Create an instance of {@link GetBicycleShopBicycleListRequest }
     * 
     */
    public GetBicycleShopBicycleListRequest createGetBicycleShopBicycleListRequest() {
        return new GetBicycleShopBicycleListRequest();
    }

    /**
     * Create an instance of {@link GetBicycleRequest }
     * 
     */
    public GetBicycleRequest createGetBicycleRequest() {
        return new GetBicycleRequest();
    }

    /**
     * Create an instance of {@link AddBicycleShopRequest }
     * 
     */
    public AddBicycleShopRequest createAddBicycleShopRequest() {
        return new AddBicycleShopRequest();
    }

    /**
     * Create an instance of {@link AddBicycleShopBicycleRequest }
     * 
     */
    public AddBicycleShopBicycleRequest createAddBicycleShopBicycleRequest() {
        return new AddBicycleShopBicycleRequest();
    }

    /**
     * Create an instance of {@link BicycleShopBicycleListType }
     * 
     */
    public BicycleShopBicycleListType createBicycleShopBicycleListType() {
        return new BicycleShopBicycleListType();
    }

    /**
     * Create an instance of {@link BicycleType }
     * 
     */
    public BicycleType createBicycleType() {
        return new BicycleType();
    }

    /**
     * Create an instance of {@link GetBicycleListResponse }
     * 
     */
    public GetBicycleListResponse createGetBicycleListResponse() {
        return new GetBicycleListResponse();
    }

    /**
     * Create an instance of {@link AddBicycleRequest }
     * 
     */
    public AddBicycleRequest createAddBicycleRequest() {
        return new AddBicycleRequest();
    }

    /**
     * Create an instance of {@link BicycleShopBicycleType }
     * 
     */
    public BicycleShopBicycleType createBicycleShopBicycleType() {
        return new BicycleShopBicycleType();
    }

    /**
     * Create an instance of {@link GetBicycleListRequest }
     * 
     */
    public GetBicycleListRequest createGetBicycleListRequest() {
        return new GetBicycleListRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleShopType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "getBicycleShopResponse")
    public JAXBElement<BicycleShopType> createGetBicycleShopResponse(BicycleShopType value) {
        return new JAXBElement<BicycleShopType>(_GetBicycleShopResponse_QNAME, BicycleShopType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleShopBicycleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "addBicycleShopBicycleResponse")
    public JAXBElement<BicycleShopBicycleType> createAddBicycleShopBicycleResponse(BicycleShopBicycleType value) {
        return new JAXBElement<BicycleShopBicycleType>(_AddBicycleShopBicycleResponse_QNAME, BicycleShopBicycleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "addBicycleResponse")
    public JAXBElement<BicycleType> createAddBicycleResponse(BicycleType value) {
        return new JAXBElement<BicycleType>(_AddBicycleResponse_QNAME, BicycleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleShopBicycleListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "getBicycleShopBicycleListResponse")
    public JAXBElement<BicycleShopBicycleListType> createGetBicycleShopBicycleListResponse(BicycleShopBicycleListType value) {
        return new JAXBElement<BicycleShopBicycleListType>(_GetBicycleShopBicycleListResponse_QNAME, BicycleShopBicycleListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "getBicycleResponse")
    public JAXBElement<BicycleType> createGetBicycleResponse(BicycleType value) {
        return new JAXBElement<BicycleType>(_GetBicycleResponse_QNAME, BicycleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BicycleShopType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", name = "addBicycleShopResponse")
    public JAXBElement<BicycleShopType> createAddBicycleShopResponse(BicycleShopType value) {
        return new JAXBElement<BicycleShopType>(_AddBicycleShopResponse_QNAME, BicycleShopType.class, null, value);
    }

}
