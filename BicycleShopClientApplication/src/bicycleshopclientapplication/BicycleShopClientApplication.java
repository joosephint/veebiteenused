/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicycleshopclientapplication;

import com.sun.prism.PixelFormat.DataType;
import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleShopRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleRequest;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 *
 * @author joosephint
 */
public class BicycleShopClientApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AddBicycleRequest addBicycle = new AddBicycleRequest();
        addBicycle.setName("Vägev ja kiire jalgratas");
        addBicycle.setColor("valge");
        addBicycle.setSize(BigInteger.ONE);
        addBicycle.setManufacturer("Merida");
        addBicycle.setModel("Crossway 300");
        addBicycle.setCode("76543");
        addBicycle.setModelYear(BigInteger.valueOf(2015));
        addBicycle.setToken("salajane");

        BicycleType addedBicycle = addBicycle(addBicycle);

        GetBicycleRequest getBicycle = new GetBicycleRequest();
        getBicycle.setToken("salajane");
        getBicycle.setId(addedBicycle.getId());

        BicycleType bicycle = getBicycle(getBicycle);

        System.out.println(bicycle.getName());
    }

    private static BicycleType addBicycle(ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleRequest parameter) {
        ee.ttu.idu0075._2015.ws.bicycleshop.BicycleService service = new ee.ttu.idu0075._2015.ws.bicycleshop.BicycleService();
        ee.ttu.idu0075._2015.ws.bicycleshop.BicyclePortType port = service.getBicyclePort();
        return port.addBicycle(parameter);
    }

    private static BicycleType getBicycle(ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleRequest parameter) {
        ee.ttu.idu0075._2015.ws.bicycleshop.BicycleService service = new ee.ttu.idu0075._2015.ws.bicycleshop.BicycleService();
        ee.ttu.idu0075._2015.ws.bicycleshop.BicyclePortType port = service.getBicyclePort();
        return port.getBicycle(parameter);
    }

}
