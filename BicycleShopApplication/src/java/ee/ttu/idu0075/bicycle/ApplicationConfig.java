/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.bicycle;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author joosephint
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {
    
    private Set<Object> singletons = new HashSet<>();
    
    public ApplicationConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setContact("Joosep Hint");
        beanConfig.setTitle("Jalgrattapoodide veebiteenus");
        beanConfig.setDescription("Jalgrattapoodide REST-põhine veebiteenus, "
                + "mis on mõeldud jalgrattapoodide ja nendega seotud jalgrataste halduseks");
        beanConfig.setScan(true);
        beanConfig.setPrettyPrint(true);
        beanConfig.setResourcePackage("ee.ttu.idu0075.bicycle");
        beanConfig.setBasePath("/BicycleShopApplication/webresources");
        beanConfig.setFilterClass("io.swagger.sample.util.ApiAuthorizationFilterImpl");
        singletons.add(beanConfig);
        singletons.add(new ApiListingResource());
        singletons.add(new SwaggerSerializers());
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        try {
            Class jsonProvider = Class.forName("org.glassfish.jersey.jackson.JacksonFeature");
            resources.add(jsonProvider);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        addRestResourceClasses(resources);
        // resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        // resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

        return resources;
    }
    
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
    
    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ee.ttu.idu0075.bicycle.BicycleShopsResource.class);
        resources.add(ee.ttu.idu0075.bicycle.BicyclesResource.class);
    }
    
}
