/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.bicycle;

import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleShopBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleShopRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopBicycleListType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopBicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleListResponse;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopBicycleListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopListResponse;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopRequest;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 *
 * @author joosephint
 */
@WebService(serviceName = "BicycleService", portName = "BicyclePort", endpointInterface = "ee.ttu.idu0075._2015.ws.bicycleshop.BicyclePortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/BicycleShop", wsdlLocation = "WEB-INF/wsdl/BicycleShopWebService/BicycleShopService.wsdl")
public class BicycleShopWebService {
    
    private static int idCounter = 0;
    
    private final static List<BicycleType> bicycles = new ArrayList<>();
    private final static List<BicycleShopType> bicycleShops = new ArrayList<>();
    private final static List<String> bicycleShopRequestCodes = new ArrayList<>();
    
    public BicycleShopWebService() {
        BicycleType firstBicycle = new BicycleType();
        firstBicycle.setId(BigInteger.valueOf(idCounter++));
        firstBicycle.setName("Suur ja võimas Merida jalgratas");
        firstBicycle.setCode("123");
        firstBicycle.setColor("white");
        firstBicycle.setManufacturer("Merida");
        firstBicycle.setModel("Crossway");
        firstBicycle.setSize(BigInteger.valueOf(32));
        firstBicycle.setModelYear(BigInteger.valueOf(2015));
        bicycles.add(firstBicycle);
        
        BicycleType secondBicycle = new BicycleType();
        secondBicycle.setId(BigInteger.valueOf(idCounter++));
        secondBicycle.setCode("434");
        secondBicycle.setManufacturer("Merida");
        secondBicycle.setModel("Reacto");
        secondBicycle.setName("Kiire Merida jalgratas");
        secondBicycle.setSize(BigInteger.valueOf(50));
        secondBicycle.setModelYear(BigInteger.valueOf(2017));
        bicycles.add(secondBicycle);
       
        BicycleShopBicycleType bicycleShopBicycle = new BicycleShopBicycleType();
        bicycleShopBicycle.setBicycle(firstBicycle);
        bicycleShopBicycle.setQuantity(BigInteger.valueOf(5));
        bicycleShopBicycle.setUnitPrice(750);
        
        BicycleShopType bicycleShop = new BicycleShopType();
        bicycleShop.setCloseHour(20);
        bicycleShop.setOpenHour(10);
        bicycleShop.setBicycleShopBicycleList(new BicycleShopBicycleListType());
        bicycleShop.setId(BigInteger.valueOf(idCounter++));
        try {
            bicycleShop.setCreatedDate(DatatypeFactory.newInstance().newXMLGregorianCalendar((GregorianCalendar) GregorianCalendar.getInstance()));
        } catch (DatatypeConfigurationException ex) {
            System.out.println(ex);
        }
        bicycleShop.setName("Veloplus");
        bicycleShop.setLocation("Pärnu maantee, Tallinn");
        
        BicycleShopBicycleListType shopListType = new BicycleShopBicycleListType();
        shopListType.getBicycleShopBicycle().add(bicycleShopBicycle);
        bicycleShop.setBicycleShopBicycleList(shopListType);
        
        bicycleShops.add(bicycleShop);
    }

    public BicycleType getBicycle(GetBicycleRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        Optional<BicycleType> bicycle = bicycles.stream()
                .filter(b -> b.getId().equals(parameter.getId()))
                .findFirst();
        if (bicycle.isPresent()) {
            return bicycle.get();
        } else {
            throw new RuntimeException("Resource not found!");
        }
    }

    public BicycleType addBicycle(AddBicycleRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        if (bicycles.stream().anyMatch(b -> b.getCode().equals(parameter.getCode()))) {
            throw new RuntimeException("Bicycle already exists!");
        }
        
        BicycleType bicycle = new BicycleType();
        
        bicycle.setCode(parameter.getCode());
        bicycle.setColor(parameter.getColor());
        bicycle.setId(BigInteger.valueOf(idCounter++));
        bicycle.setManufacturer(parameter.getManufacturer());
        bicycle.setModel(parameter.getModel());
        bicycle.setModelYear(parameter.getModelYear());
        bicycle.setName(parameter.getName());
        bicycle.setSize(parameter.getSize());
        bicycles.add(bicycle);
        
        return bicycle;
    }

    public GetBicycleListResponse getBicycleList(GetBicycleListRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        GetBicycleListResponse bicycleListResponse = new GetBicycleListResponse();
        bicycleListResponse.getBicycle().addAll(bicycles);
        
        return bicycleListResponse;
    }

    public BicycleShopType getBicycleShop(GetBicycleShopRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        Optional<BicycleShopType> bicycleShop = bicycleShops.stream()
                .filter(b -> b.getId().equals(parameter.getId()))
                .findAny();
        
        return bicycleShop.orElseThrow(() -> new RuntimeException("Bicycle shop not found!"));
    }

    public BicycleShopType addBicycleShop(AddBicycleShopRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        if (bicycleShopRequestCodes.stream().anyMatch(b -> b.equals(parameter.getRequestCode()))) {
            throw new RuntimeException("Bicycleshop already exists!");
        }
        
        BicycleShopType bicycleShop = new BicycleShopType();
        bicycleShop.setId(BigInteger.valueOf(idCounter++));
        bicycleShop.setName(parameter.getName());
        bicycleShop.setLocation(parameter.getLocation());
        bicycleShop.setOpenHour(parameter.getOpenHour());
        bicycleShop.setCloseHour(parameter.getCloseHour());
        bicycleShop.setPhoneNumber(parameter.getPhoneNumber());
        bicycleShop.setCreatedDate(parameter.getCreatedDate());
        bicycleShop.setBicycleShopBicycleList(new BicycleShopBicycleListType());
        bicycleShops.add(bicycleShop);
        bicycleShopRequestCodes.add(parameter.getRequestCode());
        return bicycleShop;
    }

    public GetBicycleShopListResponse getBicycleShopList(GetBicycleShopListRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        Stream<BicycleShopType> bicycleShopsStream = bicycleShops.stream();
        
        if (parameter.getBicycleId() != null) {
            bicycleShopsStream = bicycleShopsStream
                    .filter(bs -> hasMatchingBicycles(bs, parameter.getBicycleId()));
        }
        
        if (parameter.getCurrentHour() != null) {
            bicycleShopsStream = bicycleShopsStream
                    .filter(b -> b.getOpenHour() < parameter.getCurrentHour())
                    .filter(b -> b.getCloseHour() > parameter.getCurrentHour());
        }
        
        if (parameter.getHasAnyBicyclesInStore() != null) {
            Boolean hasAnyInStore = parameter.getHasAnyBicyclesInStore().equals("yes");
            bicycleShopsStream = bicycleShopsStream
                    .filter(b -> b.getBicycleShopBicycleList().getBicycleShopBicycle()
                            .isEmpty() != hasAnyInStore);
        }
        
        GetBicycleShopListResponse response = new GetBicycleShopListResponse();
        response.getBicycleShop().addAll(bicycleShopsStream.collect(Collectors.toList()));
        
        return response;
    }
    
    private boolean hasMatchingBicycles(BicycleShopType bs, BigInteger bicycleId) {
        return bs.getBicycleShopBicycleList().getBicycleShopBicycle().stream()
                            .filter(b -> b.getBicycle().getId().equals(bicycleId))
                            .count() > 0L;
    }

    public BicycleShopBicycleListType getBicycleShopBicycleList(GetBicycleShopBicycleListRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        Optional<BicycleShopType> bicyleShop = bicycleShops.stream()
                .filter(b -> b.getId().equals(parameter.getBicycleShopId()))
                .findAny();
        if (bicyleShop.isPresent()) {
            return bicyleShop.get().getBicycleShopBicycleList();
        } else {
            throw new RuntimeException("Bicycle shop not found!");
        }
    }

    public BicycleShopBicycleType addBicycleShopBicycle(AddBicycleShopBicycleRequest parameter) {
        if (!isValidToken(parameter.getToken())) {
            throw new RuntimeException("Invalid token!");
        }
        
        Optional<BicycleType> bicycle = bicycles.stream()
                .filter(b -> b.getId().equals(parameter.getBicycleId()))
                .findAny();
        Optional<BicycleShopType> bicycleShop = bicycleShops.stream()
                .filter(b -> b.getId().equals(parameter.getBicycleShopId()))
                .findAny();
        
        if (bicycle.isPresent() && bicycleShop.isPresent()) {
            BicycleShopBicycleType bicycleShopBicycle = new BicycleShopBicycleType();
            Optional<BicycleShopBicycleType> opt = bicycleShop.get().getBicycleShopBicycleList().getBicycleShopBicycle().stream()
                    .filter(b -> b.getBicycle().getId().equals(bicycle.get().getId()))
                    .findFirst();
            
            if (opt.isPresent()) {
                bicycleShopBicycle = opt.get();
            }
            
            bicycleShopBicycle.setBicycle(bicycle.get());
            bicycleShopBicycle.setQuantity(parameter.getQuantity());
            bicycleShopBicycle.setUnitPrice(parameter.getUnitPrice());
            
            bicycleShop.get().getBicycleShopBicycleList()
                    .getBicycleShopBicycle().add(bicycleShopBicycle);
            
            return bicycleShopBicycle;
        }
        if (!bicycle.isPresent()) {
            throw new RuntimeException("Bicycle id not found!");
        }
        throw new RuntimeException("Bicycle shop id not found!");
    }
    
    public static final Boolean isValidToken(String token) {
        return token != null && token.equalsIgnoreCase("salajane");
    }
    
    public static final List<BicycleType> getBicycles() {
        return bicycles;
    }
    
    public static final List<BicycleShopType> getBicycleShops() {
        return bicycleShops;
    }
    
    public static final Integer getNextId() {
        return idCounter++;
    }
    
}
