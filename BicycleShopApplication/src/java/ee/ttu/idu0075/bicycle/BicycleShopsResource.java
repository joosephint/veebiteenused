/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.bicycle;

import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleShopBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleShopRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopBicycleListType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopBicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleShopType;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopBicycleListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopListResponse;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleShopRequest;
import static ee.ttu.idu0075.bicycle.BicycleShopWebService.isValidToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author joosephint
 */
@Path("bicycleShops")
@Api("Jalgrattapoodide teenus")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BicycleShopsResource {

    @Context
    private UriInfo context;

    private BicycleShopWebService service;

    /**
     * Creates a new instance of BicycleShopsResource
     */
    public BicycleShopsResource() {
        this.service = new BicycleShopWebService();
    }

    @GET
    @Path("{id : \\d+}")
    @ApiOperation("Tagastab jalgratta unikaalse identifikaatori alusel")
    public BicycleShopType getBicycleShop(@PathParam("id") BigInteger id,
            @ApiParam(required = true) @QueryParam("token") String token) {
        GetBicycleShopRequest request = new GetBicycleShopRequest();
        request.setId(id);
        request.setToken(token);

        return service.getBicycleShop(request);
    }

    @PUT
    @ApiOperation("Lisab uue jalgrattapoe süsteemi")
    public BicycleShopType addBicycleShop(AddBicycleShopRequest request) {
        return service.addBicycleShop(request);
    }

    @GET
    @Path("list")
    @ApiOperation("Tagastab jalgrattapoodide nimekirja etteantud filtreeringute alusel")
    public GetBicycleShopListResponse getBicycleShopList(
            @ApiParam(type = "Etteantud kellaaja tund", allowableValues = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23") @QueryParam("currentHour") Integer currentHour,
            @ApiParam(type = "Jalgratta unikaalne identifikaator") @QueryParam("bicycleId") BigInteger bicycleId,
            @ApiParam(type = "Kas jalgrattaid on poes või mitte", allowableValues = "yes,no") @QueryParam("hasAnyBicyclesInStore") String hasAnyBicyclesInStore,
            @ApiParam(required = true) @QueryParam("token") String token) {
        if (currentHour < 0 || currentHour > 23) {
            throw new RuntimeException("Vale hetketund, lubatud on 0 - 23");
        }
        
        if (hasAnyBicyclesInStore != null && 
                !Arrays.asList("yes", "no").contains(hasAnyBicyclesInStore)) {
            throw new RuntimeException("Vale hasBicyclesInStore valikuvariant, lubatud on yes|no");
        }
        
        GetBicycleShopListRequest request = new GetBicycleShopListRequest();
        request.setCurrentHour(currentHour);
        request.setHasAnyBicyclesInStore(hasAnyBicyclesInStore);
        request.setBicycleId(bicycleId);
        request.setToken(token);
        
        return service.getBicycleShopList(request);
    }

    @GET
    @Path("bicycle-list/{id : \\d+}")
    @ApiOperation("Tagastab etteantud poes olevate jalgrataste nimekirja")
    public BicycleShopBicycleListType getBicycleShopBicycleList(
            @ApiParam(type = "Jalgrattapoe unikaalne identifikaator", required = true) @PathParam("id") BigInteger bicycleShopId,
            @ApiParam(required = true) @QueryParam("token") String token) {
        GetBicycleShopBicycleListRequest request = new GetBicycleShopBicycleListRequest();
        request.setBicycleShopId(bicycleShopId);
        request.setToken(token);
        
        return service.getBicycleShopBicycleList(request);
    }

    @PUT
    @Path("add-bicycle")
    @ApiOperation("Lisab uue seose jalgratta ja jalgrattapoe vahel")
    public BicycleShopBicycleType addBicycleShopBicycle(AddBicycleShopBicycleRequest request) {
        if (request.getQuantity().compareTo(BigInteger.ONE) < 0) {
            throw new RuntimeException("Vale kogus");
        }
        
        if (request.getUnitPrice() < 0) {
            throw new RuntimeException("Vale ratta hind");
        }
               
        return service.addBicycleShopBicycle(request);
    }
}
