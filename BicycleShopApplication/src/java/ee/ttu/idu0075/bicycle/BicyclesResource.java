/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.bicycle;

import ee.ttu.idu0075._2015.ws.bicycleshop.AddBicycleRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.BicycleType;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleListRequest;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleListResponse;
import ee.ttu.idu0075._2015.ws.bicycleshop.GetBicycleRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.math.BigInteger;
import java.util.Optional;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author joosephint
 */
@Path("bicycles")
@Api("Jalgrataste teenus")
@Consumes(MediaType.APPLICATION_JSON) 
@Produces(MediaType.APPLICATION_JSON)
public class BicyclesResource {

    @Context
    private UriInfo context;
    
    private BicycleShopWebService service;

    /**
     * Creates a new instance of BicyclesResource
     */
    public BicyclesResource() {
        this.service = new BicycleShopWebService();
    }

    /**
     * Retrieves representation of an instance of ee.ttu.idu0075.bicycle.BicyclesResource
     * @return an instance of ee.ttu.idu0075._2015.ws.bicycleshop.BicycleType
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Tagastab kõikide jalgrataste nimekirja")
    public GetBicycleListResponse getBicycleList(@ApiParam(required = true) @QueryParam("token") String token) {
        GetBicycleListRequest request = new GetBicycleListRequest();
        request.setToken(token);
        
        return service.getBicycleList(request);
    }
    
    @GET
    @Path("{id : \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Tagastab jalgratta unikaalse identifikaatori alusel")
    public BicycleType getBicycle(@PathParam("id") BigInteger id,
           @ApiParam(required = true) @QueryParam("token") String token) {
        GetBicycleRequest request = new GetBicycleRequest();
        request.setId(id);
        request.setToken(token);
        
        return service.getBicycle(request);
    }
    
    @PUT
    @ApiOperation("Lisab uue jalgratta ja tagastab selle")
    public BicycleType addBicycle(AddBicycleRequest request) {
        return service.addBicycle(request);
    }
}
